gia_theo_ngay = [10.1, 9.5, 10.7, 10.8, 10.6, 10.0, 10.2, 9.5]


def mua_co_phieu():
    for index, gia in enumerate(gia_theo_ngay):
        current_element = gia
        next_element = gia_theo_ngay[(index + 1) % len(gia_theo_ngay)]
        if next_element == current_element + (current_element * 0.07):
            print('Ban co phieu.')
        elif next_element == current_element - (current_element * 0.06):
            print('Mua co phieu.')
        else:
            print('Do nothing.')


mua_co_phieu()


if __name__ == '__main__':
    mua_co_phieu()
